# Boilerplate for websocket game #

Built with [Express Generator](https://github.com/expressjs/generator) and [Socket.IO](http://socket.io/)
## [Demo](http://nodejs-socketgame.rhcloud.com/) ##

### What is this repository for? ###

* Building a mobile multiplayer campaign game

### How to use? ###

* Clone the repo
* Run `npm install`
* Start server by running `npm start` - this will run the app with debug logging on
* Game logic in routes/index.js