var app = (function (){

    var playerId;

    var socket = io( /*'http://localhost:3000'*/ );

    var room = window._room;

    var gameStarted = false;


    return {

        init: function () {

            socket.on('connect', function() {
                console.log('connected..joining ' + room);

                var joinData = {
                    room: room
                };

                if(window.localStorage.getItem('playerId') !== null) {

                    joinData.playerId = window.localStorage.getItem('playerId');
                }
                socket.emit('join', joinData);

            });
            // when over player limit for current room
            socket.on('newGame', function() {
                window.location.replace('/');
            });

            socket.on('assign', function(data) {
                console.log(data);
                app.setPlayerId(data.pid);
            });

            socket.on('playerEntered', function (data) {

                if(data.playerCount === 2 && !gameStarted) {

                    app.initGame();
                }
            });


        },
        setPlayerId: function(id) {

            playerId = id;

            window.localStorage.setItem('playerId', playerId);

        },
        initGame: function () {

            /**
             * Game logic
             */
            gameStarted = true;

            socket.on('message', function(message) {

                $('#window').append('<div class="bubble bubble--other">' + message + '</div>');
                console.log('message received!!');

            });

            $('#playerId').text(playerId);

            $('#chat').keypress(function(e) {

                var message = $(this).val();

                if (e.which == 13) {

                    $('#window').append('<div class="bubble bubble--self">' + message + '</div>');

                    socket.emit('message', message);

                    $(this).val('');

                    return false;
                }
            });

        }

    }

 })();


app.init();

