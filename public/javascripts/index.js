(function () {

    var socket = io( /*'http://localhost:3000'*/ );

    var room = window._room;

    socket.on('connect', function() {
        console.log('connected');
        
        window.localStorage.removeItem('playerId');

    });

    socket.on('beginGame', function(data) {

        console.log('begin game!')

        if (room === data.room) {
            window.location.replace('/' + room);
        }
    });

})();

