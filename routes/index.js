var express = require('express');
var router = express.Router();

var helpers = require('../helpers');

module.exports = function(io) {
    


    //todo: store list of game/room id and redirect on server?

    /* GET home page. */
    router.get('/', function(req, res, next) {

        var room = helpers.generateRoom(6),
            url = req.protocol + '://' + req.get('host') + req.path + room,
            svgString = helpers.qr.imageSync(url, { type: 'svg' });

        res.render('index', {
            title: 'Express',
            room: room,
            shareURL: url,
            qr: svgString
        });
    });

    router.get('/:room([A-Za-z0-9]{6})', function(req, res) {
        var room = req.params.room;
        res.render('game', {
            title: 'Express',
            room: room
        });


    });

    io.on('connection', function(socket) {

        var room;

        socket.on('join', function (data) {

            console.log('**** joined ****');

            var rooms = socket.adapter.rooms;
                room = data.room,
                playerId = data.playerId;


            if(!rooms[data.room]) {

                socket.join(data.room, function (err) {

                    if (err) return err;

                    console.log('in room ' + data.room + ' with ' +  rooms[data.room].length + ' players');

                    io.emit('beginGame', {room: data.room}); // makes first player leave QR screen to game 

                    socket.emit('assign', {pid: playerId? playerId : rooms[data.room].length});

                    io.to(data.room).emit('playerEntered', {playerCount: rooms[data.room].length});

                });


            } else if (rooms[data.room].length <=1) {


                socket.join(data.room, function (err) {

                    if (err) return err;

                    console.log('in room ' + data.room + ' with ' +  rooms[data.room].length + ' players');

                    socket.emit('assign', {pid: playerId ? playerId : rooms[data.room].length});                    
                    io.to(data.room).emit('playerEntered', {playerCount: rooms[data.room].length});

                });

            } else  {

                console.log('cannot join this room - make a new room');
                socket.emit('newGame');

            }

        });


        socket.on('disconnect', function () {
            console.log('disconnected!!');
        });

        /**
         * Game logic
        **/

        socket.on('message', function onMessage (data) {
            console.log(data);

            if(typeof room !== 'undefined') {
                socket.to(room).emit('message', data); // sending to all clients except sender
                //io.to(data.room).emit('message', data);  // sending to all clients, include sender
            }

        });

    });



    return router;

}